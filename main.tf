
terraform {
  required_version = ">= 0.12"
}

resource "gitlab_label" "fixme" {
  project     = "rigrassm/dotbot-yum"
  name        = "fixme"
  description = "issue with failing tests"
  color       = "#ffcc00"
}
